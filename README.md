# Simple Banking Application

A Simple banking application where you can create account, 
get account details, deposit and withdraw money.

![model](images/classdiagram.png)

## Postman Test Collection

[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/ea39df197cf3616ceb6f)
