package io.gitlab.alpertoy.simplebanking.service;

import io.gitlab.alpertoy.simplebanking.exception.AccountNotFoundException;
import io.gitlab.alpertoy.simplebanking.model.Account;
import io.gitlab.alpertoy.simplebanking.model.SavableAccount;
import io.gitlab.alpertoy.simplebanking.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * User: alpertoy
 * Date: 25.02.2021
 * Time: 00:16
 */

@Service
public class AccountService {

    @Autowired
    private AccountRepository accountRepository;

    public Account findAccount(String accountNumber) {

        return accountRepository.findById(accountNumber)
                .orElseThrow(() -> new AccountNotFoundException(accountNumber));
    }

    public Account createAccount(SavableAccount savableAccount) {

        Account account = new Account(savableAccount.getOwner(), savableAccount.getAccountNumber());
        return accountRepository.save(account);
    }

    public void saveAccount(Account account) {

        accountRepository.save(account);
    }
}
