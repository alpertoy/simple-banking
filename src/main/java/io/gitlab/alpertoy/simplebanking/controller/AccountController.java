package io.gitlab.alpertoy.simplebanking.controller;

import io.gitlab.alpertoy.simplebanking.model.*;
import io.gitlab.alpertoy.simplebanking.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * User: alpertoy
 * Date: 25.02.2021
 * Time: 15:40
 */

@RestController()
@RequestMapping("/account/v1")
public class AccountController {

    @Autowired
    private AccountService accountService;

    @GetMapping("{account-number}")
    public ResponseEntity<Account> getAccount(@PathVariable(value = "account-number") final String accountNumber) {

        return new ResponseEntity<>(accountService.findAccount(accountNumber), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Account> createAccount(@RequestBody final SavableAccount savableAccount) {

        return new ResponseEntity<>(accountService.createAccount(savableAccount), HttpStatus.ACCEPTED);
    }

    @PostMapping("credit/{account-number}")
    public ResponseEntity<TransactionStatus> credit(@PathVariable(value = "account-number") final String accountNumber,
                                                    @RequestBody DepositTransaction transaction) {

        TransactionStatus transactionStatus = executeTransaction(accountNumber, transaction);

        return new ResponseEntity<>(transactionStatus, HttpStatus.ACCEPTED);
    }

    @PostMapping("debit/{account-number}")
    public ResponseEntity<TransactionStatus> debit(@PathVariable(value = "account-number") final String accountNumber,
                                                   @RequestBody WithdrawalTransaction transaction) {

        TransactionStatus transactionStatus = executeTransaction(accountNumber, transaction);

        return new ResponseEntity<>(transactionStatus, HttpStatus.ACCEPTED);
    }

    private TransactionStatus executeTransaction(String accountNumber, Transaction transaction) {

        Account account = accountService.findAccount(accountNumber);
        account.post(transaction);
        accountService.saveAccount(account);

        TransactionStatus transactionStatus = new TransactionStatus();
        transactionStatus.setToFinishedState();
        transactionStatus.setApprovalCode(transaction.getApprovalCode());
        return transactionStatus;
    }
}
