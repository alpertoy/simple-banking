package io.gitlab.alpertoy.simplebanking.model;

/**
 * User: alpertoy
 * Date: 24.02.2021
 * Time: 22:51
 */
public class SavableAccount {

    private String accountNumber;
    private String owner;

    public SavableAccount() {

    }

    public SavableAccount(String accountNumber, String owner) {

        this.accountNumber = accountNumber;
        this.owner = owner;
    }

    public String getAccountNumber() {

        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {

        this.accountNumber = accountNumber;
    }

    public String getOwner() {

        return owner;
    }

    public void setOwner(String owner) {

        this.owner = owner;
    }
}
