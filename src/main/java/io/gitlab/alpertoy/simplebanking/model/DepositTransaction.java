package io.gitlab.alpertoy.simplebanking.model;

import javax.persistence.Entity;
import java.util.Date;

/**
 * User: alpertoy
 * Date: 24.02.2021
 * Time: 13:43
 */

@Entity
public class DepositTransaction extends Transaction {

    private double amount;

    public DepositTransaction() {

        super(new Date());
        this.amount = 0;
    }

    public DepositTransaction(double amount) {

        super(new Date());
        this.amount = amount;
    }

    @Override
    public double getAmount() {

        return amount;
    }

    @Override
    public String getType() {

        return "DepositTransaction";
    }

    public void setAmount(double amount) {

        this.amount = amount;
    }

    @Override
    public String toString() {

        return "Deposit transaction, date: " + getDate() + " amount: " + amount;
    }
}
