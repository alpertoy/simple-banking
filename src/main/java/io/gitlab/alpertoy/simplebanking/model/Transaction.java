package io.gitlab.alpertoy.simplebanking.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import java.util.Date;
import java.util.UUID;

/**
 * User: alpertoy
 * Date: 24.02.2021
 * Time: 13:26
 */

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public abstract class Transaction {

    private Date date;
    @Id
    private String approvalCode;

    public Transaction() {

        date = new Date();
        approvalCode = UUID.randomUUID().toString();
    }

    public Transaction(Date date) {

        this();
        this.date = date;

    }

    public Date getDate() {

        return date;
    }

    public void setDate(Date date) {

        this.date = date;
    }

    public String getApprovalCode() {

        return approvalCode;
    }

    public void setApprovalCode(String approvalCode) {

        this.approvalCode = approvalCode;
    }

    public abstract double getAmount();

    public abstract String getType();
}
