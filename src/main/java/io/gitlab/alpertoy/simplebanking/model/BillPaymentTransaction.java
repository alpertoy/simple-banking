package io.gitlab.alpertoy.simplebanking.model;

/**
 * User: alpertoy
 * Date: 24.02.2021
 * Time: 21:08
 */
public class BillPaymentTransaction extends WithdrawalTransaction {

    private String serviceProvider;
    private String phoneNumber;

    public BillPaymentTransaction() {

    }

    public BillPaymentTransaction(String serviceProvider, String phoneNumber) {

        this.serviceProvider = serviceProvider;
        this.phoneNumber = phoneNumber;
    }

    public BillPaymentTransaction(double amount, String serviceProvider, String phoneNumber) {

        super(amount);
        this.serviceProvider = serviceProvider;
        this.phoneNumber = phoneNumber;
    }

    public String getServiceProvider() {

        return serviceProvider;
    }

    public void setServiceProvider(String serviceProvider) {

        this.serviceProvider = serviceProvider;
    }

    public String getPhoneNumber() {

        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {

        this.phoneNumber = phoneNumber;
    }

    @Override
    public String getType() {

        return "BillPaymentTransaction";
    }
}
