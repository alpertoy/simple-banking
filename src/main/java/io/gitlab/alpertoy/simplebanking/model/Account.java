package io.gitlab.alpertoy.simplebanking.model;

import io.gitlab.alpertoy.simplebanking.exception.InsufficientBalanceException;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * User: alpertoy
 * Date: 24.02.2021
 * Time: 13:31
 */

@Entity
public class Account {

    private String owner;
    @Id
    private String accountNumber;
    private double balance;
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<Transaction> transactions;

    public Account() {

        transactions = new ArrayList<>();

    }

    public Account(String owner, String accountNumber) {

        this.owner = owner;
        this.accountNumber = accountNumber;
        transactions = new ArrayList<>();
    }

    public String getOwner() {

        return owner;
    }

    public void setOwner(String owner) {

        this.owner = owner;
    }

    public String getAccountNumber() {

        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {

        this.accountNumber = accountNumber;
    }

    public double getBalance() {

        return balance;
    }

    public void setBalance(double balance) {

        this.balance = balance;
    }

    public List<Transaction> getTransactions() {

        return transactions;
    }

    public void setTransactions(List<Transaction> transactions) {

        this.transactions = transactions;
    }

    public void deposit(double amount) {

        executeTransaction(amount);

    }

    public void withdraw(double amount) {

        executeTransaction(-amount);
    }

    public void post(Transaction transaction) {

        executeTransaction(transaction.getAmount());
        this.transactions.add(transaction);
    }

    private void executeTransaction(double amount) {

        if (balance + amount < 0) {
            throw new InsufficientBalanceException();
        }

        balance += amount;
    }
}
