package io.gitlab.alpertoy.simplebanking.model;

import javax.persistence.Entity;
import java.util.Date;

/**
 * User: alpertoy
 * Date: 24.02.2021
 * Time: 14:48
 */

@Entity
public class WithdrawalTransaction extends Transaction {

    private double amount;

    public WithdrawalTransaction() {

        super(new Date());
        this.amount = 0;
    }

    public WithdrawalTransaction(double amount) {

        super(new Date());
        this.amount = amount;
    }

    @Override
    public double getAmount() {

        return -amount;
    }

    @Override
    public String getType() {

        return "WithdrawalTransaction";
    }

    public void setAmount(double amount) {

        this.amount = amount;
    }

    @Override
    public String toString() {

        return "Withdrawal transaction, date: " + getDate() + " amount: " + amount;
    }
}
