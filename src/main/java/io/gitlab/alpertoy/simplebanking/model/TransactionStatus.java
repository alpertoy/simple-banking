package io.gitlab.alpertoy.simplebanking.model;

/**
 * User: alpertoy
 * Date: 25.02.2021
 * Time: 15:41
 */
public class TransactionStatus {

    private String status;
    private String approvalCode;

    public TransactionStatus() {

        this.setToPendingState();
    }

    public void setToPendingState() {

        this.status = "PENDING";
    }

    public void setToFinishedState() {

        this.status = "OK";

    }

    public String getStatus() {

        return status;
    }

    public String getApprovalCode() {

        return approvalCode;
    }

    public void setApprovalCode(String approvalCode) {

        this.approvalCode = approvalCode;
    }
}
