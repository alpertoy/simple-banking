package io.gitlab.alpertoy.simplebanking.exception;

/**
 * User: alpertoy
 * Date: 24.02.2021
 * Time: 13:33
 */
public class InsufficientBalanceException extends RuntimeException  {

    public InsufficientBalanceException() {

        super("Insufficient balance in the account to execute given transaction");
    }
}
