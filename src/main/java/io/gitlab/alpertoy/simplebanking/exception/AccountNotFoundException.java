package io.gitlab.alpertoy.simplebanking.exception;

/**
 * User: alpertoy
 * Date: 25.02.2021
 * Time: 00:17
 */


public class AccountNotFoundException extends RuntimeException {

    public AccountNotFoundException(String accountNumber) {

        super("Account number not found : " + accountNumber);
    }
}
