package io.gitlab.alpertoy.simplebanking.repository;

import io.gitlab.alpertoy.simplebanking.model.Account;
import org.springframework.data.repository.CrudRepository;

/**
 * User: alpertoy
 * Date: 24.02.2021
 * Time: 23:54
 */
public interface AccountRepository extends CrudRepository<Account, String> {
}
