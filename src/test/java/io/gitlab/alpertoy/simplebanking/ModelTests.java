package io.gitlab.alpertoy.simplebanking;

import io.gitlab.alpertoy.simplebanking.exception.InsufficientBalanceException;
import io.gitlab.alpertoy.simplebanking.model.Account;
import io.gitlab.alpertoy.simplebanking.model.BillPaymentTransaction;
import io.gitlab.alpertoy.simplebanking.model.DepositTransaction;
import io.gitlab.alpertoy.simplebanking.model.WithdrawalTransaction;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * User: alpertoy
 * Date: 25.02.2021
 * Time: 16:29
 */
public class ModelTests {

    @Test
    public void testCreateAccountAndSetBalance0() {
        Account account = new Account("Alper Toy", "17892");
        assertTrue(account.getOwner().equals("Alper Toy"));
        assertTrue(account.getAccountNumber().equals("17892"));
        assertTrue(account.getBalance() == 0);
    }

    @Test
    public void testDepositIntoBankAccount() {
        Account account = new Account("John Doe", "9834");
        account.deposit(100);
        assertTrue(account.getBalance() == 100);
    }

    @Test
    public void testWithdrawFromBankAccount() throws InsufficientBalanceException {
        Account account = new Account("John Doe", "9834");
        account.deposit(100);
        assertTrue(account.getBalance() == 100);
        account.withdraw(50);
        assertTrue(account.getBalance() == 50);
    }

    @Test
    public void testWithdrawException() {
        Assertions.assertThrows( InsufficientBalanceException.class, () -> {
            Account account = new Account("John Doe", "9834");
            account.deposit(100);
            account.withdraw(500);
        });

    }

    @Test
    public void testTransactions() throws InsufficientBalanceException {
        // Create account
        Account account = new Account("Melissa Johnson", "1234");
        assertTrue(account.getTransactions().size() == 0);

        // Deposit Transaction
        DepositTransaction depositTrx = new DepositTransaction(100);
        assertTrue(depositTrx.getDate() != null);
        account.post(depositTrx);
        assertTrue(account.getBalance() == 100);
        assertTrue(account.getTransactions().size() == 1);

        // Withdrawal Transaction
        WithdrawalTransaction withdrawalTrx = new WithdrawalTransaction(60);
        assertTrue(withdrawalTrx.getDate() != null);
        account.post(withdrawalTrx);
        assertTrue(account.getBalance() == 40);
        assertTrue(account.getTransactions().size() == 2);
    }

    @Test
    public void testBIllTransaction() {
        Account account = new Account("Tom", "12345");
        account.post(new DepositTransaction(1000));
        account.post(new WithdrawalTransaction(200));
        account.post(new BillPaymentTransaction(96.50, "T-Mobile", "5423345566"));
        assertEquals(account.getBalance(), 703.50, 0.0001);
    }
}
